<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /*
         * Nombre alumno: Raquel Pont
         * Ejercicio: php2.php
         * Enunciado: Crea un programa que cuente las letras "a" (mayúsculas o
         * minúsculas) que existan en una cadena de texto.
         * Ejecución: El texto contiene 6 letras 'a' o 'A'.
         */
        
        $cadena = "Esto es una cadena de prueba para el ejercicio php2.php";
        $contador = 0;
        $letrasA= 0;
        $cadena = strtoupper($cadena);
        while ($contador < strlen($cadena)) {
            if ($cadena[$contador] == "A") {
                $letrasA++;
            }
            $contador++;
        }
        echo "<p>El texto contiene $letrasA letras 'a' o 'A'.</p>"
        ?>
    </body>
</html>