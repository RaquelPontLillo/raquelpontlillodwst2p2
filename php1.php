<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /*
         * Nombre alumno: Raquel Pont
         * Ejercicio: php1.php
         * Enunciado: Crea un programa que imprima por pantalla los múltiplos de
         * 3 del 1 al 100.
         * Ejecución: 3 6 9 12... (hasta 99)
         */
        
        for ($i = 3; $i < 100; $i = $i+3) {
            echo "<p>";
            echo $i;
            echo "</p>";
        }
        
        ?>
    </body>
</html>
