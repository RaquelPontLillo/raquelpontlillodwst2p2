<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /*
         * Nombre alumno: Raquel Pont
         * Ejercicio: php3.php
         * Enunciado: Crea un programa que cuente las letras o caracteres que 
         * hay en un texto pero sin contar los espacios en blanco.
         * Ejecución: El texto contiene 46 letras o caracteres.
         */
        
        $cadena = "   Esto es una cadena de prueba para el ejercicio php3.php    ";
        $contador = 0;
        $letras= 0;
        $cadena = trim($cadena);
        
        do {
            if ($cadena[$contador] != " ") {
                $letras++;
            }
            $contador++;
        } while ($contador < strlen($cadena));
        
        echo "<p>El texto contiene $letras letras o caracteres.</p>"
        ?>
    </body>
</html>